import express from 'express';
import bodyParser from 'body-parser';
import deliveryRoutes from './routes/delivery';
import cors from './util/cors';

const app = express();

app.set("view engine","ejs");
app.set('views', 'views');

app.use(cors);

app.use(bodyParser.urlencoded({ type: 'application/*+json' }));
app.use(bodyParser.json());

app.use('/delivery', deliveryRoutes);

app.listen(8888);