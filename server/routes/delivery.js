import express from 'express';
import { postDeliveryState } from '../controllers/delivery';

const router = express.Router();

// /product GET
router.post('/', postDeliveryState);

export default router;