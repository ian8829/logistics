import axios from 'axios';

export default class Products {
    constructor(t_invoice, t_code) {
        this.t_invoice = t_invoice;
        this.t_code = t_code;
    }

    getStatus() {
        const t_key = 'MzOpaXFdqA4IYzL3XXPkQQ';
        const params = { t_invoice: this.t_invoice, t_code: this.t_code, t_key };

        return axios.get('/api/v1/trackingInfo', {
            baseURL: 'http://info.sweettracker.co.kr',
            params,
            headers: {
                'content-type': 'application/json;charset=UTF-8',
                'accept': 'application/json;charset=UTF-8',
            }
        });
    }
}