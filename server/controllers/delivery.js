import Delivery from '../models/delivery';

// /product POST
export const postDeliveryState = (req, res, next) => {
    const { t_invoice, t_code } = req.body;
    const delivery = new Delivery(t_invoice, t_code);

    delivery.getStatus()
        .then((response) => {
            res.json(response.data);
        })
        .catch((err) => { 
                res.sendStatus(500);
                console.log('error for fetch delivery data', err);
            } 
        );
};