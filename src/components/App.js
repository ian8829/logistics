import React from 'react';
import { Router, Route } from 'react-router-dom';
import DeliverySearch from './delivery/DeliverySearch';
import DeliveryStatus from './delivery/DeliveryStatus';
import history from '../history';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"

const App = () => {
    return (
        <div className="ui container">
            <Router history={history}>
                <div>
                    <Route path="/" exact component={DeliverySearch} />
                    <Route path="/delivery/status" exact component={DeliveryStatus} />
                </div>
            </Router>
        </div>
    );
};

export default App;