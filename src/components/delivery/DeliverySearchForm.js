import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';

class DeliverySearchForm extends Component {
    renderError({ error, touched }) {
        if (touched && error) {
            return (
                <div className="ui error message">
                    <div className="header">{error}</div>
                </div>
            )
        }
    }

    renderField = ({ input, label, meta, children }) => {
        const className = `field ${meta.error && meta.touched ? 'error' : ''}`;
        const Input = <input {...input} autoComplete="off" />;
        const Select = (
            <select 
                {...input} 
                style={{ height: '38px', textIndent: '0.5em' }}
            >
                {children}
            </select>
        );

        return (
            <div className={className}>
                <label>{label}</label>
                {label === "택배사" ? Select : Input}
                {this.renderError(meta)}
            </div>
        );
    }

    onSubmit = (formValues) => {
        this.props.onSubmit(formValues);
    }

    render() {
        return (
            <form 
                onSubmit={this.props.handleSubmit(this.onSubmit)}
                className="ui form error"
            >
                <Field 
                    name="t_invoice" 
                    component={this.renderField} 
                    label="운송장 번호"
                />
                <Field
                    name="t_code" 
                    component={this.renderField}
                    label="택배사"
                >
                    <option />
                    <option value="04">CJ대한통운</option>
                    <option value="05">한진택배</option>
                    <option value="23">경동택배</option>
                </Field>
                <button className="ui button primary">
                    조회하기
                </button>
            </form>
        );
    }
}

const validate = (formValues) => {
    const errors = {};

    if (!formValues.t_invoice) {
        errors.t_invoice = '운송장 번호를 입력해 주세요.';
    }
    if (!formValues.t_code) {
        errors.t_code = '택배사를 입력해 주세요.';
    }
    return errors;
}

export default reduxForm({
    form: 'deliverySearchForm',
    validate: validate
})(DeliverySearchForm);