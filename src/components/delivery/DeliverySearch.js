import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { fetchDeliveryStatus, cleanDeliveryStatus } from '../../actions';
import DeliverySearchForm from './DeliverySearchForm';
import history from '../../history';
import Modal from '../Modal';

class DeliverySearch extends Component {
    onSubmit = (formValues) => {
        this.props.fetchDeliveryStatus(formValues);
    };

    renderActions() {
        return (
            <React.Fragment>
                <Link 
                    to="/" 
                    className="ui button"
                    onClick={() => this.props.cleanDeliveryStatus()}
                >
                확인
                </Link>
            </React.Fragment>
        );
    }

    renderContent() {
        if (this.props.deliveryInfos) {
            return this.props.deliveryInfos.msg;
        }
        return 'Fetch Error';
    }

    renderModal() {
        return (
            <Modal 
                title="조회 실패"
                content={this.renderContent()}
                actions={this.renderActions()}
                onDismiss={() => history.push('/')}
            />
        );
    }

    render() {
        const { deliveryInfos } = this.props;
        return (
            <div style={{ marginTop: '30px' }}>
                <h3>배송 정보 입력</h3>
                <DeliverySearchForm onSubmit={this.onSubmit} />
                {(deliveryInfos && !deliveryInfos.result) ? this.renderModal() : null}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return { 
        deliveryInfos: state.deliveryInfos,
    };
};

export default connect(mapStateToProps, { 
    fetchDeliveryStatus,
    cleanDeliveryStatus
 })(DeliverySearch);