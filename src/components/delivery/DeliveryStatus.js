import React, { Component } from 'react';
import { connect } from 'react-redux';
import Loader from 'react-loader-spinner'
import { Link } from 'react-router-dom';
import { fetchDeliveryStatus, cleanDeliveryStatus } from '../../actions';

class DeliveryStatus extends Component {
    componentDidMount() {
        const { deliveryInfos } = this.props;
        const t_code = localStorage.getItem('t_code');
        const t_invoice = localStorage.getItem('t_invoice');

        if (!deliveryInfos) {
            this.props.fetchDeliveryStatus({ t_code, t_invoice });
        }
    }

    renderTrackingDetails() {
        return this.props.deliveryInfos.trackingDetails.map(trackingDetail => {
            return (
                <div className="item" key={trackingDetail.time}>
                    <i className="marker icon"></i>
                    <div className="content">
                        {trackingDetail.kind} / {trackingDetail.where} ({trackingDetail.timeString})
                    </div>
                </div>
            );
        });
    }

    render() {
        const { deliveryInfos } = this.props;
        if (!deliveryInfos) {
            return (
                <div style={{ marginTop: '200px', textAlign: 'center' }}>
                    <Loader
                        type="Puff"
                        color="#00BFFF"
                        height={100}
                        width={100}
                    />
                </div>
            )
        }
        const { itemName, senderName, receiverName, invoiceNo, receiverAddr, complete } = deliveryInfos;
        return (
            <div className="ui raised very padded text container segment" style={{ marginTop: '30px' }}>
                <div className="card">
                    <div className="content">
                        <h2>배송 현황</h2>
                        <p>송장번호: {invoiceNo}</p>
                        <p>상품명: {itemName}</p>
                        <p>보내는 사람: {senderName}</p>
                        <p>받는 사람: {receiverName}</p>
                        <p>송장번호: {invoiceNo}</p>
                        <p>배송주소: {receiverAddr}</p>
                        <p>배송상태: {!complete ? "배송중" : "배송완료"}</p>
                    </div>
                    <div className="ui list">
                        {this.renderTrackingDetails()}
                    </div>
                    <Link 
                        to="/" 
                        className="ui button primary"
                        onClick={() => this.props.cleanDeliveryStatus()}
                    >
                        뒤로가기
                    </Link>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return { 
        deliveryInfos: state.deliveryInfos
    };
};

export default connect(
    mapStateToProps, 
    { 
        fetchDeliveryStatus,
        cleanDeliveryStatus 
    }
)(DeliveryStatus);