import { 
    FETCH_DELIVERY, 
    FETCH_DELIVERY_FAIL, 
    CLEAN_DELIVERY_STATUS 
} from '../actions/types';

export default (state = null, action) => {
    switch (action.type) {
        case FETCH_DELIVERY_FAIL:
            return { ...state, ...action.payload };
        case FETCH_DELIVERY:
            return { ...state, ...action.payload };
        case CLEAN_DELIVERY_STATUS:
            return null;
        default:
            return state;
    }
}