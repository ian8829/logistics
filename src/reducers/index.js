import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import deliveryReducer from './deliveryReducer';

export default combineReducers({
    form: formReducer,
    deliveryInfos: deliveryReducer
});