import delivery from '../apis/delivery';
import history from '../history';
import { FETCH_DELIVERY, FETCH_DELIVERY_FAIL, CLEAN_DELIVERY_STATUS } from "./types";

export const fetchDeliveryStatus = (formValues) => async (dispatch, getState) => {
    const response = await delivery.post('/delivery', { ...formValues });

    if (response) {
        if (!response.data.result) {
           return dispatch({ type: FETCH_DELIVERY_FAIL, payload: response.data });
        }
        localStorage.setItem('t_code', formValues.t_code);
        localStorage.setItem('t_invoice', formValues.t_invoice);
        dispatch({ type: FETCH_DELIVERY, payload: response.data });
        history.push('/delivery/status');
    } else {
        dispatch({ type: FETCH_DELIVERY_FAIL });
    }
};

export const cleanDeliveryStatus = () => {
    localStorage.clear();
    return {
        type: CLEAN_DELIVERY_STATUS
    };
};